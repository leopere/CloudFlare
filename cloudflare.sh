#!/bin/sh

## Load config
if [ -L $0 ]
then
	source `readlink -e $0 | sed "s:[^/]*$:config:"`
else
	source `echo $0 | sed "s:[^/]*$:config:"`
fi

## Added Functions
case $1 in
  getdns )
    curl https://www.cloudflare.com/api_json.html \
      -d 'a=rec_load_all' \
      -d "tkn=$AUTH_KEY" \
      -d "email=$EMAIL_ADDR" \
      -d "z=$DOMAIN_NAME"|jq
    ;;
  update )
    [ ! -f ./currentip ] && touch ./currentip

    ## Original Suggestion for resolving external IP
    ## NEWIP=`dig +short myip.opendns.com @resolver1.opendns.com`
    NEWIP=`curl https://ifconfig.co`
    CURRENTIP=`cat /var/tmp/currentip.txt`

    if [ "$NEWIP" = "$CURRENTIP" ]
    then
      echo "IP address unchanged"
    else
      curl https://www.cloudflare.com/api_json.html \
        -d 'a=rec_edit' \
        -d "tkn=$AUTH_KEY" \
        -d email="$EMAIL_ADDR" \
        -d "z=$DOMAIN_NAME" \
        -d "id=RECID" \
        -d 'type=A' \
        -d "name=dyn.$DOMAIN_NAME" \
        -d 'ttl=1' \
        -d "content=$NEWIP"
      echo $NEWIP > /var/tmp/currentip
    fi
    ;;
  * )
    echo "usage: ./cloudflare.sh getdns|update"
  ;;
esac

#!/bin/bash
if [ -L $0 ]
then
	source `readlink -e $0 | sed "s:[^/]*$:config:"`
else
	source `echo $0 | sed "s:[^/]*$:config:"`
fi

case $1 in
	zones )
		curl -X GET "https://api.cloudflare.com/client/v4/zones?name=$DOMAIN_NAME&status=active&page=1&per_page=20&order=status&direction=desc&match=all"\
		-H "X-Auth-Email: $EMAIL_ADDR"\
		-H "X-Auth-Key: $AUTH_KEY"\
		-H "Content-Type: application/json"
		;;
	update )
		curl -X PUT "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$ZONE_ID"\
		-H "X-Auth-Email: $EMAIL_ADDR"\
		-H "X-Auth-Key: $AUTH_KEY"\
		-H "Content-Type: application/json"\
		--data '{"id":"$ZONE_ID","type":"A","name":"$DOMAIN_NAME","content":"1.2.3.4","proxiable":true,"proxied":false,"ttl":120,"locked":false,"zone_id":"$ZONE_ID","zone_name":"$DOMAIN_NAME","created_on":"2014-01-01T05:20:00.12345Z","modified_on":"2014-01-01T05:20:00.12345Z","data":{}}'
		;;
	list )
		curl -X GET "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records?type=A&name=$DOMAIN_NAME&content=127.0.0.1&page=1&per_page=20&order=type&direction=desc&match=all"\
		-H "X-Auth-Email: $EMAIL_ADDR"\
		-H "X-Auth-Key: $AUTH_KEY"\
		-H "Content-Type: application/json"
		;;
	* )
		echo "Usage: ./cloudflare.sh [zones|update|list]"
		;;
esac
